module.exports = {

  head: {
    title: 'training',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: 'Nuxt.js project'}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ],
  },

  cache: true,

  modules: [
    '@nuxtjs/dotenv',
    '@nuxtjs/axios',
    '@nuxtjs/component-cache',

    // ['@nuxtjs/component-cache', {
    //   max: 10000,
    //   maxAge: 1000 * 60 * 60
    // }],

    ['bootstrap-vue/nuxt', {css: false}],
    [
      'nuxt-sass-resources-loader',
      [
        '@/assets/scss/main.scss'
      ]
    ]
  ],

  router: {
    linkExactActiveClass: 'active-link'
  },

  axios: {
    baseURL: 'https://pokeapi.co/api/v2',
    retry: {
      retries: 3
    },
  },

  loading: {
    color: '#3B8070'
  },

  css: [
    '~/assets/scss/main.scss',
  ],

  build: {
    extend(config, {isDev, isClient}) {

      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

